<?php
App::uses('AppModel', 'Model');

class Menu extends AppModel {

	public $displayField = 'name';

    public $hasMany = array(
        'SubMenu' => array(
            'className' => 'SubMenu',
            'foreignKey' => 'menu_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => array('SubMenu.order'),
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

}
