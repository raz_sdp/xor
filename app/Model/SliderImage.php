<?php
App::uses('AppModel', 'Model');

class SliderImage extends AppModel {

	public $hasMany = array(
		'SliderLayer' => array(
			'className' => 'SliderLayer',
			'foreignKey' => 'slider_image_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => array('SliderLayer.order'),
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
