<?php
App::uses('AppModel', 'Model');
/**
 * CustomOrder Model
 *
 */
class CustomOrder extends AppModel {
    public $belongsTo = array(
        'Customer' => array(
            'className' => 'Customer',
            'foreignKey' => 'customer_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
}
