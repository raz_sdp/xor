<?php
App::uses('AppController', 'Controller');

class TeamMembersController extends AppController {

	public $components = array('Paginator', 'Session');

	public function admin_index() {
		$this->TeamMember->recursive = 0;
		$this->set('teamMembers', $this->Paginator->paginate());
	}

	public function admin_add() {
		if ($this->request->is('post')) {
            if (!empty($this->request->data['TeamMember']['image_link'])) {
                $file_name = $this->_upload($this->request->data['TeamMember']['image_link'], 'team');
                $this->request->data['TeamMember']['image_link'] = $file_name;
            } else {
                unset($this->request->data['TeamMember']['image_link']);
            }
            $this->TeamMember->create();
            if ($this->TeamMember->save($this->request->data)) {
				$this->Session->setFlash(__('The team member has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The team member could not be saved. Please, try again.'));
			}
		}
	}

	public function admin_edit($id = null) {
		if (!$this->TeamMember->exists($id)) {
			throw new NotFoundException(__('Invalid team member'));
		}
		if ($this->request->is(array('post', 'put'))) {
            if (!empty($this->request->data['TeamMember']['image_link'])) {
                if(!empty($this->request->data['TeamMember']['image_link']['name'])){
                    $file_name = $this->_upload($this->request->data['TeamMember']['image_link'], 'team');
                    $this->request->data['TeamMember']['image_link'] = $file_name;
                } else {
                    $options = array('conditions' => array('TeamMember.' . $this->TeamMember->primaryKey => $id));
                    $data = $this->TeamMember->find('first', $options);
                    $this->request->data['TeamMember']['image_link'] = $data['TeamMember']['image_link'];
                }
            } else {
                unset($this->request->data['TeamMember']['image_link']);
            }
			if ($this->TeamMember->save($this->request->data)) {
				$this->Session->setFlash(__('The team member has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The team member could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('TeamMember.' . $this->TeamMember->primaryKey => $id));
			$this->request->data = $this->TeamMember->find('first', $options);
		}
	}

	public function admin_delete($id = null) {
		$this->TeamMember->id = $id;
		if (!$this->TeamMember->exists()) {
			throw new NotFoundException(__('Invalid team member'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->TeamMember->delete()) {
			$this->Session->setFlash(__('The team member has been deleted.'));
		} else {
			$this->Session->setFlash(__('The team member could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
