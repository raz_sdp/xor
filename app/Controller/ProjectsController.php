<?php
App::uses('AppController', 'Controller');

class ProjectsController extends AppController {

	public $components = array('Paginator', 'Session');

	public function admin_index() {
		$this->Project->recursive = 0;
		$this->set('projects', $this->Paginator->paginate());
	}

	public function admin_add() {
		if ($this->request->is('post')) {
            if (!empty($this->request->data['Project']['image_link'])) {
                $file_name = $this->_upload($this->request->data['Project']['image_link'], 'projects');
                $this->request->data['Project']['image_link'] = $file_name;
            } else {
                unset($this->request->data['Project']['image_link']);
            }
			$this->Project->create();
			if ($this->Project->save($this->request->data)) {
				$this->Session->setFlash(__('The project has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project could not be saved. Please, try again.'));
			}
		}
	}

	public function admin_edit($id = null) {
		if (!$this->Project->exists($id)) {
			throw new NotFoundException(__('Invalid project'));
		}
		if ($this->request->is(array('post', 'put'))) {
            $this->Project->id = $id;
            if (!empty($this->request->data['Project']['image_link'])) {
                if(!empty($this->request->data['Project']['image_link']['name'])){
                    $file_name = $this->_upload($this->request->data['Project']['image_link'], 'projects');
                    $this->request->data['Project']['image_link'] = $file_name;
                } else {
                    $options = array('conditions' => array('Project.' . $this->Project->primaryKey => $id));
                    $data = $this->Project->find('first', $options);
                    $this->request->data['Project']['image_link'] = $data['Project']['image_link'];
                }
            } else {
                unset($this->request->data['Project']['image_link']);
            }
			if ($this->Project->save($this->request->data)) {
				$this->Session->setFlash(__('The project has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Project.' . $this->Project->primaryKey => $id));
			$this->request->data = $this->Project->find('first', $options);
		}
	}

	public function admin_delete($id = null) {
		$this->Project->id = $id;
		if (!$this->Project->exists()) {
			throw new NotFoundException(__('Invalid project'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Project->delete()) {
			$this->Session->setFlash(__('The project has been deleted.'));
		} else {
			$this->Session->setFlash(__('The project could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

    public function admin_add_to_portfolio($id = null) {
        $this->Project->id = $id;
        if (!$this->Project->exists()) {
            throw new NotFoundException(__('Invalid project'));
        }

        $this->loadModel('Portfolio');
        $is_exist = $this->Portfolio->findByProjectId($id);
        if(!empty($is_exist)){
            $this->Session->setFlash(__('The project is already in the portfolio.'));
        } else {
            $data = array('project_id'=>$id);
            $this->Portfolio->create();
            if ($this->Portfolio->save($data)) {
                $this->Session->setFlash(__('The project has been added to portfolio.'));
            } else {
                $this->Session->setFlash(__('The project could not be added to portfolio. Please, try again.'));
            }
        }
        return $this->redirect(array('action' => 'index'));
    }
}
