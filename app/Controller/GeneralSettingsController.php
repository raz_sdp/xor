<?php
App::uses('AppController', 'Controller');

class GeneralSettingsController extends AppController {

	public $components = array('Paginator', 'Session');

    //
    // can't create new settings or delete it. edit only.
    //

    /*
    public function admin_index() {
		$this->GeneralSetting->recursive = 0;
		$this->set('generalSettings', $this->Paginator->paginate());
	}

	public function admin_view($id = null) {
		if (!$this->GeneralSetting->exists($id)) {
			throw new NotFoundException(__('Invalid general setting'));
		}
		$options = array('conditions' => array('GeneralSetting.' . $this->GeneralSetting->primaryKey => $id));
		$this->set('generalSetting', $this->GeneralSetting->find('first', $options));
	}

	public function admin_add() {
		if ($this->request->is('post')) {
			$this->GeneralSetting->create();
			if ($this->GeneralSetting->save($this->request->data)) {
				$this->Session->setFlash(__('The general setting has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The general setting could not be saved. Please, try again.'));
			}
		}
	}

    public function admin_delete($id = null) {
		$this->GeneralSetting->id = $id;
		if (!$this->GeneralSetting->exists()) {
			throw new NotFoundException(__('Invalid general setting'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->GeneralSetting->delete()) {
			$this->Session->setFlash(__('The general setting has been deleted.'));
		} else {
			$this->Session->setFlash(__('The general setting could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
    */


	public function admin_edit($id = null) {
		if (!$this->GeneralSetting->exists($id)) {
			throw new NotFoundException(__('Invalid general setting'));
		}
		if ($this->request->is(array('post', 'put'))) {
            if (!empty($this->request->data['GeneralSetting']['logo'])) {
                if(!empty($this->request->data['GeneralSetting']['logo']['name'])){
                    $file_name = $this->_upload($this->request->data['GeneralSetting']['logo'], 'logos');
                    $this->request->data['GeneralSetting']['logo'] = $file_name;
                } else {
                    $options = array('conditions' => array('GeneralSetting.' . $this->GeneralSetting->primaryKey => $id));
                    $data = $this->GeneralSetting->find('first', $options);
                    $this->request->data['GeneralSetting']['logo'] = $data['GeneralSetting']['logo'];
                }
            } else {
                unset($this->request->data['GeneralSetting']['logo']);
            }

			if ($this->GeneralSetting->save($this->request->data)) {
				$this->Session->setFlash(__('The general setting has been saved.'));
				//return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The general setting could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('GeneralSetting.' . $this->GeneralSetting->primaryKey => $id));
			$this->request->data = $this->GeneralSetting->find('first', $options);
		}
	}
}
