<?php echo $this->Html->css('custom'); ?>
<?php echo $this->element('menu'); ?>
<div class="teamMembers form">
<?php echo $this->Form->create('TeamMember', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Team Member'); ?></legend>
	<?php
		echo $this->Form->input('full_name');
		echo $this->Form->input('display_name');
        echo $this->Form->input('image_link', array('type' => 'file'));
		echo $this->Form->input('designation');
		echo $this->Form->input('work_role');
		echo $this->Form->input('description');
		echo $this->Form->input('contact_1');
		echo $this->Form->input('contact_2');
		echo $this->Form->input('mail_1');
		echo $this->Form->input('mail_2');
		echo $this->Form->input('active');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Team Members'), array('action' => 'index')); ?></li>
	</ul>
</div>
