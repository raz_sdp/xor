<?php echo $this->Html->css('custom'); ?>
<?php echo $this->element('menu'); ?>
<div class="sliderImages index slider_general">
    <?php echo $this->Form->create('SliderGeneralSetting'); ?>
        <fieldset>
            <legend><?php echo __('Edit General Setting'); ?></legend>
            <?php
            echo $this->Form->input('delay');
            echo $this->Form->input('start_width');
            echo $this->Form->input('start_height');
            echo $this->Form->input('hide_thumbs');
            ?>
        </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>

    <section class="link-skewed">
        <p>Reverse General Settings to <?php echo $this->Html->link(__('Default'), array('action' => 'default_general_setting')); ?></p>
    </section>

</div>