<?php echo $this->Html->css('custom'); ?>
<?php echo $this->element('menu'); ?>
<?php $slider_id = $this->request->data['SliderLayer']['slider_image_id']; ?>
<div class="sliderLayers form">
<?php echo $this->Form->create('SliderLayer', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Slider Layer of slider id: '); echo $slider_id;?></legend>
	<?php
        echo $this->Form->input('slider_image_id', array('type' => 'hidden', 'value' => $slider_id));
		echo $this->Form->input('order');
		echo $this->Form->input('class');
		echo $this->Form->input('data-x');
		echo $this->Form->input('data-hoffset');
		echo $this->Form->input('data-y');
		echo $this->Form->input('data-voffset');
		echo $this->Form->input('data-customin');
		echo $this->Form->input('data-customout');
		echo $this->Form->input('data-speed');
		echo $this->Form->input('data-start');
		echo $this->Form->input('data-easing');
		echo $this->Form->input('data-endspeed');
		echo $this->Form->input('data-end');
		echo $this->Form->input('data-endeasing');
		echo $this->Form->input('data-captionhidden');
    ?>
        <div class="input text">
            <label for="ContentType">Content Type</label>
            <select name="data[SliderLayer][content_type]">
                <option <?php if( $this->request->data['SliderLayer']['content_type'] == "txt" ) echo "selected";?> value="txt">Text</option>
                <option <?php if( $this->request->data['SliderLayer']['content_type'] == "img" ) echo "selected";?> value="img">Image</option>
                <option <?php if( $this->request->data['SliderLayer']['content_type'] == "vdo" ) echo "selected";?> value="vdo">Video</option>
            </select>
        </div>
    <?php
		echo $this->Form->input('text');
        if(!empty($this->request->data['SliderLayer']['image'])){
            echo "<div class=\"thumbnail-item\">";
            echo $this->Html->image('/files/' . $this->request->data['SliderLayer']['image'], array('alt'=>'picture', 'width'=> '150px','height' => '150px'));
            echo "</div>";
        }
        echo $this->Form->input('image', array('type' => 'file'));
		echo $this->Form->input('image_width');
		echo $this->Form->input('image_height');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
        <li><?php echo $this->Html->link(__('Add a Layer'), array('controller' => 'sliderImages', 'action' => 'add_layer', $slider_id)); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete_layer', $this->Form->value('SliderLayer.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('SliderLayer.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Slider Layers'), array('action' => 'layers', $slider_id)); ?></li>
		<li><?php echo $this->Html->link(__('List Slider Images'), array('action' => 'index')); ?> </li>
	</ul>
</div>
