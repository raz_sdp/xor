<style type="text/css">
.header_large  {
    width:100%;
    height:170px;
    background: url(images/header.jpg) repeat-x ;
    position: fixed;
    z-index: 9999;
    transition: all 0.7s;
}
.header_large .site_title{
    padding-top: 68px !important;
    transition: all 0.7s;
}
.header_large .logo {
    padding: 45px 0 0 0 !important;
    transition: all 0.7s;
}
.header_large .logo  img {
    width: 100px !important;
    height: 100px !important;
    transition: all 0.7s;
}
.header_large .logo-right-side{
      padding-top: 66px !important;
      transition: all 0.7s;
 }
.header_large .title { 
      font-size: 35px !important;
      transition: all 0.7s;
}
.header_small {
    width: 100%;
    height: 118px;
    background: url(images/header_mini.png) repeat-x;
    position: fixed;
    z-index: 9999;
    transition: all 0.7s;
}
.header_small .logo {
    padding: 30px 0 0 0 !important;
    transition: all 0.7s;
}
.header_small .logo  img {
    width: 65px !important;
    height: 65px !important;
    transition: all 0.7s;
}
.header_small .logo-right-side  {
    padding-top: 42px !important;
    transition: all 0.7s;
}
 .header_small .cell-mini {
    padding-bottom: 5px !important;
    transition: all 0.7s;
}
.header_small .title {
    font-size: 25px !important;
    transition: all 0.7s;
}

 .header_small .site_title {
   
    padding-top: 39px !important;
    transition: all 0.7s;
}
.header_small .border_right {
    height: 30px !important;
     transition: all 0.7s;
}
.header_small .ddsmoothmenu ul li a {
    width: 80px !important;
    line-height: 18px !important;
    height: 30px !important;
    font-size: 14px !important;
     transition: all 0.7s;
} 
.header_small .menu {

   height: 44px !important;
    transition: all 0.7s;
    background: url("images/menushed1.png") no-repeat scroll center bottom transparent !important;
}
 .head-line   {
    margin-right: 15px;
    margin-left: 15px;
}

</style>


<div id="sticky_header" class="templatemo_header_mini header_large">

   
        <div class="row head-line">
            <div class="col-md-4 col-xs-12">
                <div class="logo new-logo">
                    <a href="#home"><?php echo $this->Html->image('../files/logos/'.$general_setting['GeneralSetting']['logo']);?></a>
                </div>
                <div class="site_title new-site">
                    <span class="title new-title"><?php echo $general_setting['GeneralSetting']['company_name'];?></span>
                    <div class="cleaner"></div>
                    <span class="quote"><?php echo $general_setting['GeneralSetting']['slogan'];?></span>
                </div>
            </div>
            <!-- top left side -->
            <div class="col-md-8 col-xs-12">
                <div class="logo-right-side new-logo-right-side">
                   
                   <!-- <nav class="navbar navbar-inverse">
                      <div class="container-fluid">
                        <div class="navbar-header">
                          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>                        
                          </button>
                          <a class="navbar-brand" href="#">WebSiteName</a>
                        </div>
                        <div class="collapse navbar-collapse" id="myNavbar">
                          <ul class="nav navbar-nav">
                            <li class="active"><a href="#">Home</a></li>
                            <li class="dropdown">
                              <a class="dropdown-toggle" data-toggle="dropdown" href="#">Page 1 <span class="caret"></span></a>
                              <ul class="dropdown-menu">
                                <li><a href="#">Page 1-1</a></li>
                                <li><a href="#">Page 1-2</a></li>
                                <li><a href="#">Page 1-3</a></li>
                              </ul>
                            </li>
                            <li><a href="#">Page 2</a></li>
                            <li><a href="#">Page 3</a></li>
                          </ul>
                          <ul class="nav navbar-nav navbar-right">
                            <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                          </ul>
                        </div>
                      </div>
                    </nav> -->

                    <div class="cleaner"></div>
                    <nav class="navbar">
                        <div class="navbar-header">
                          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#templatemo_menu">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>                        
                          </button>
                        </div>
                        <div class="menu pull-right">
                            <div class="collapse navbar-collapse ddsmoothmenu" id="templatemo_menu">
                                <ul class="nav navbar-nav">
                                    <?php foreach ($menues as $key => $menue) { //AuthComponent::_setTrace($menue); ?>
                                        <li class="active">
                                            <a href="<?php echo $this->Html->url($menue['Menu']['link'])?>" class="selected"><?php echo $menue['Menu']['name'];
                                            ?>
                                            </a>
                                        </li>
                                        <?php
                                        if($key<count($menues)-1)
                                            echo '<div class="border_right"></div>';
                                    }
                                    ?>
                                </ul>
                                <br style="clear: left" />
                            </div> <!-- end of templatemo_menu -->
                        </div>
                    </nav>
                    <div class="cleaner"></div>
                </div>
            </div>
            <!-- top right side -->
        </div>
    </div>
            </div>  <!-- END of templatemo_header_wrapper -->
