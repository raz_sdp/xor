<?php
#AuthComponent::_setTrace($menues);
?>



            <div id="main_header" class="templatemo_header_wrapper">            
                
                    <div class="row">
                        <div class="col-md-4 col-xs-12">
                            <div class="logo">
                                <?php echo $this->Html->image('../files/logos/'.$general_setting['GeneralSetting']['logo'],array('width'=>'100','height'=>'100'));?>
                            </div>
                            <div class="site_title">
                                <span class="title"><?php echo $general_setting['GeneralSetting']['company_name'];?></span>
                                <div class="cleaner"></div>
                                <span class="quote"><?php echo $general_setting['GeneralSetting']['slogan'];?></span>
                            </div>
                        </div>
                        <!-- top left side -->
                        <div class="col-md-8 col-xs-12">
                            <div class="logo-right-side">
                                <div class="cell pull-right">
                                    <div class="country_icon"></div>
                                    <span class="cell_no"><?php echo $contact_information['ContactInformation']['phone_1'];?></span>
                                </div>
                                <div class="cleaner"></div>
                                <div class="menu pull-right">
                                    <div id="templatemo_menu" class="ddsmoothmenu">
                                        <ul>
                                        <?php                                        
                                            foreach ($menues as $key => $menue) {
                                                //AuthComponent::_setTrace($menue);
                                               ?>
                                                <li class="active">
                                                    <a href="<?php echo $this->Html->url($menue['Menu']['link'])?>" class="selected"><?php echo $menue['Menu']['name'];
                                                    ?>
                                                    </a>
                                                </li>
                                                <?php
                                                if($key<count($menues)-1)
                                                    echo '<div class="border_right"></div>';
                                                ?>                                            
                                            <?php
                                            }
                                        ?>                                           
                                        </ul>
                                        <br style="clear: left" />
                                    </div> <!-- end of templatemo_menu -->
                                </div>
                                <div class="cleaner"></div>
                            </div>
                        </div>
                        <!-- top right side -->
                    </div>
                
            </div>  <!-- END of templatemo_header_wrapper -->

