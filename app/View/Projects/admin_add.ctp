<?php echo $this->Html->css('custom'); ?>
<?php echo $this->element('menu'); ?>
<div class="projects form">
<?php echo $this->Form->create('Project', array('type'=>'file')); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Project'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('status');
		echo $this->Form->input('description');
		echo $this->Form->input('features');
		echo $this->Form->input('project_type');
		echo $this->Form->input('image_link', array('type'=>'file'));
        echo $this->Form->input('client_name');
        echo $this->Form->input('client_organization');
        echo $this->Form->input('client_testimonial');
		echo $this->Form->input('live_demo_link');
		echo $this->Form->input('google_link');
		echo $this->Form->input('apple_link');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Projects'), array('action' => 'index')); ?></li>
	</ul>
</div>
