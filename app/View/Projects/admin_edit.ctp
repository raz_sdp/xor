<?php echo $this->Html->css('custom'); ?>
<?php echo $this->element('menu'); ?>
<div class="projects form">
<?php echo $this->Form->create('Project', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Project'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('status');
        echo $this->Form->input('short_desc');
		echo $this->Form->input('description');
		echo $this->Form->input('features');
		echo $this->Form->input('project_type');
        if(!empty($this->Form->data['Project']['image_link'])){
            echo "<div class=\"thumbnail-item\">";
            echo $this->Html->image('/files/projects/' . $this->request->data['Project']['image_link'], array('alt'=>'picture', 'width'=> '150px','height' => 'auto'));
            echo "</div>";
        }
        echo $this->Form->input('image_link', array('type' => 'file'));
        echo $this->Form->input('client_name');
        echo $this->Form->input('client_organization');
        echo $this->Form->input('client_testimonial');
		echo $this->Form->input('live_demo_link');
		echo $this->Form->input('google_link');
		echo $this->Form->input('apple_link');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Project.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Project.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Projects'), array('action' => 'index')); ?></li>
	</ul>
</div>
