<?php echo $this->Html->css('custom'); ?>
<?php echo $this->element('menu'); ?>
<div class="users form">
<?php echo $this->Form->create('User'); ?>
	<fieldset>
		<legend><?php echo __('Add User'); ?></legend>
	<?php
		echo $this->Form->input('username');
		echo $this->Form->input('simple_pwd');
	?>
        <div class="input text">
            <label for="UserRole">User Role</label>
            <select name="data[User][role]">
                <option value="admin">Admin</option>
                <option value="member">Member</option>
            </select>
        </div>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?></li>
	</ul>
</div>