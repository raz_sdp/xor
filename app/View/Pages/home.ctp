<style type="text/css">
    .carousel-control.left  {
        background-image: none !important;
    }
    .carousel-control.right {
        background-image: none !important;
    }
    @media screen and (min-width: 768px){
        .carousel-control .glyphicon-chevron-left, .carousel-control .icon-prev {
            margin-left: -135px;
        }
    }
    @media screen and (min-width: 768px){
    .carousel-control .glyphicon-chevron-right, .carousel-control .icon-next {
        margin-right: -135px;
       }
    }
</style>
<?php #AuthComponent::_setTrace($data); ?>

<!-- slider content start -->
<section id="home">
    <div id="slider_preview" class="slider_preview" >
    <?php echo $this->Html->css(array('style','settings')); ?>
    <?php echo $this->Html->script(array('jquery', 'jquery.themepunch.tools.min', 'jquery.themepunch.revolution.min')); ?>
    <article class="boxedcontainer">
        <div class="tp-banner-container">
            <div class="tp-banner" >

                <ul>
                    <?php

                    foreach($data as $key => $slide){

                        echo "<li data-transition=\"". $slide['SliderImage']['transition_type'] . "\" data-slotamount=\"7\" data-masterspeed=\"" . $slide['SliderImage']['speed'] . "\" >";

                        $main_image = $slide['SliderImage']['image_link'];
                        echo $this->Html->image($main_image, array('alt'=>"", 'style'=>"background-color:#b2c4cc", 'data-bgfit'=>"cover", 'data-bgposition'=>"left top", 'data-bgrepeat'=>"no-repeat"));

                        foreach($slide['SliderLayer'] as $layer) {
                            echo "<div class=\"" . $layer['class'] . "\"" ;
                            if(!empty($layer['data-x'])) echo " data-x=\"". $layer['data-x'] . "\"";
                            if(!empty($layer['data-hoffset'])) echo " data-hoffset=\"". $layer['data-hoffset'] . "\"";
                            if(!empty($layer['data-y'])) echo " data-y=\"". $layer['data-y'] . "\"";
                            if(!empty($layer['data-voffset'])) echo " data-voffset=\"". $layer['data-voffset'] . "\"";
                            if(!empty($layer['data-customin'])) echo " data-customin=\"". $layer['data-customin'] . "\"";
                            if(!empty($layer['data-customout'])) echo " data-customout=\"". $layer['data-customout'] . "\"";
                            if(!empty($layer['data-speed'])) echo " data-speed=\"". $layer['data-speed'] . "\"";
                            if(!empty($layer['data-start'])) echo " data-start=\"". $layer['data-start'] . "\"";
                            if(!empty($layer['data-easing'])) echo " data-easing=\"". $layer['data-easing'] . "\"";
                            if(!empty($layer['data-endspeed'])) echo " data-endspeed=\"". $layer['data-endspeed'] . "\"";
                            if(!empty($layer['data-end'])) echo " data-end=\"". $layer['data-end'] . "\"";
                            if(!empty($layer['data-endeasing'])) echo " data-endeasing=\"". $layer['data-endeasing'] . "\"";
                            if(!empty($layer['data-captionhidden'])) echo " data-captionhidden=\"". $layer['data-captionhidden'] . "\"";
                            if($layer['content_type'] == 'vdo') {
                                echo " data-autoplay=\"false\"";
                                echo " data-autoplayonlyfirsttime=\"false\"";
                            }
                            $z = ($key == 0) ? ($layer['order'] + 2) : ($layer['order'] + 1) ;
                            echo " style=\"z-index: ". $z . ";\"";
                            echo " >";

                            if($layer['content_type'] == 'img') {
                                $image_options = array('alt'=>"");
                                if(!empty($layer['image_width']))
                                    $image_options['data-ww'] = $layer['image_width'];
                                if(!empty($layer['image_height']))
                                    $image_options['data-hh'] = $layer['image_height'];

                                echo $this->Html->image($layer['image'], $image_options);
                            } else {
                                echo $layer['text'];
                            }

                            echo "</div>";
                        }

                        echo "</li>";

                    }

                    ?>
                </ul>

                <div class="tp-bannertimer"></div>

            </div>
        </div>

        <!-- THE SCRIPT INITIALISATION -->
        <!-- LOOK THE DOCUMENTATION FOR MORE INFORMATIONS -->
        <script type="text/javascript">
            var revapi;
            jQuery(document).ready(function() {
                revapi = jQuery('.tp-banner').revolution(
                    {
                        delay: <?php echo $general['SliderGeneralSetting']['delay'];?>,
                        startwidth: <?php echo $general['SliderGeneralSetting']['start_width'];?>,
                        startheight: <?php echo $general['SliderGeneralSetting']['start_height'];?>,
                        hideThumbs: <?php echo $general['SliderGeneralSetting']['hide_thumbs'];?>,
                        fullWidth: "on",
                        forceFullWidth: "on"
                    });

            });
        </script>
        <!-- END REVOLUTION SLIDER -->
    </article>
</div>
</section>
<!-- Slider End -->

<!-- services start -->
<section id="services" class="our-services section-padding">
    <div class="container">
        <div class="row text-center">
            <div class="col-xs-12">
                <h2 class="section-title wow zoomIn">What We Do</h2>
            </div>
        </div> <!-- /.row -->

        <div class="row content-row">
            <div class="col-xs-12 col-sm-6 service-content">
                <div class="media">

                    <a class="media-left" href="#">
                       <!--  <i class="flaticon-bulb11"></i> -->
                       <img style="width:300px;" src="img/graphic1.png">
                    </a>

                    <div class="media-body">
                        <h3 class="media-heading"><?php echo $services[0]['Service']['name'];?></h3>
                        <p><?php echo $services[0]['Service']['description'];?></p>

                        <a class="btn btn-primary" href="#">Read More</a>
                    </div> <!-- /.media-body -->
                </div> <!-- /.media -->
            </div> <!-- /.col-sm-6 -->


            <div class="col-xs-12 col-sm-6 service-content">
                <div class="media">

                    <a class="media-left" href="#">
                      <!--   <i class="flaticon-window22"></i> -->
                       <img style="width:375px; height: 78px;" src="img/weblogo.png">
                    </a>

                    <div class="media-body">
                        <h3 class="media-heading"><?php echo $services[1]['Service']['name'];?></h3>
                        <p><?php echo $services[1]['Service']['description'];?></p>

                        <a class="btn btn-primary" href="#">Read More</a>
                    </div> <!-- /.media-body -->
                </div> <!-- /.media -->
            </div> <!-- /.col-sm-6 -->

            <div class="col-xs-12 col-sm-6 service-content">
                <div class="media">

                    <a class="media-left" href="#">
                       <!--  <i class="flaticon-speaker30"></i> -->
                        <img style="width:300px;" src="img/add.png">
                    </a>

                    <div class="media-body">
                        <h3 class="media-heading"><?php echo $services[2]['Service']['name'];?></h3>
                        <p><?php echo $services[2]['Service']['description'];?></p>

                        <a class="btn btn-primary" href="#">Read More</a>
                    </div> <!-- /.media-body -->
                </div> <!-- /.media -->
            </div> <!-- /.col-sm-6 -->


            <div class="col-xs-12 col-sm-6 service-content">
                <div class="media">

                    <a class="media-left" href="#">
                        <!-- <i class="flaticon-bars6"></i> -->
                        <img style="width:380px;height: 83px;" src="img/custom.png">
                    </a>

                    <div class="media-body">
                        <h3 class="media-heading"><?php echo $services[3]['Service']['name'];?></h3>
                        <p><?php echo $services[3]['Service']['description'];?></p>

                        <a class="btn btn-primary" href="#">Read More</a>
                    </div> <!-- /.media-body -->
                </div> <!-- /.media -->
            </div> <!-- /.col-sm-6 -->
        </div> <!-- /.row -->

    </div><!-- /.container -->
</section>
<!-- services end -->

<!-- process start -->
<section id="about" class="working-process section-padding">
    <div class="container">
        <div class="row text-center">
            <div class="col-xs-12">
                <h2 class="section-title wow zoomIn">Our Working Process</h2>
            </div>
        </div> <!-- /.row -->

        <div class="row text-center circle-connector">
            <div class="col-xs-6 col-sm-3">
                <div class="process-circle wow zoomIn">
                    <a href="#"><img src="img/meet.png" alt=""></a>
                    <span>Meet</span>
                </div><!-- /.circle -->
            </div><!-- /.col-sm-3 -->
            <div class="col-xs-6 col-sm-3">
                <div class="process-circle wow zoomIn">
                    <a href="#"><img src="img/resharch.png" alt=""></a>
                    <span>Research</span>
                </div><!-- /.circle -->
            </div><!-- /.col-sm-3 -->
            <div class="col-xs-6 col-sm-3">
                <div class="process-circle wow zoomIn">
                    <a href="#"><img style="width:72%;" src="img/idea.png" alt=""></a>
                    <span>Cultivate Ideas</span>
                </div><!-- /.circle -->
            </div><!-- /.col-sm-3 -->
            <div class="col-xs-6 col-sm-3">
                <div class="process-circle wow zoomIn">
                    <a href="#"><img src="img/met5.png" alt=""></a>
                    <span>Solve Problems</span>
                </div><!-- /.circle -->
            </div><!-- /.col-sm-3 -->
        </div><!-- /.row -->


        <div class="star-devider">
            <img src="img/black-star.png" alt="">
        </div>
        <div class="row text-center" style="margin-top: 80px;">
            <div class="col-xs-12">
                <h2 class="section-title wow zoomIn">We Make Beautiful Website</h2>
                <div class="team-intro wow zoomIn">
                    <p>Completely re-engineer superior catalysts for change without team driven schemas. Synergistically myocardinate backward-compatible bandwidth and B2B platforms. Appropriately reconceptualize leveraged expertise for go forward action items. Compellingly incentivize progressive catalysts for change whereas leading-edge meta-services. Assertively leverage existing market positioning outsourcing for effective architectures.</p>
                </div>
            </div>
        </div> <!-- /.row -->
    </div>
</section>
<!-- process end -->

<!-- work start -->
<section id="portfolio" class="work-section section-padding">
    <div class="container">
        <div class="row text-center">
            <div class="col-xs-12">
                <h2 class="section-title wow zoomIn">A Section of Our Work</h2>
            </div>
        </div> <!-- /.row -->

        <div class="work-filter text-center">
            <ul class="list-inline" id="filter">
                <li><a class="active" data-group="all">All</a></li>
                <li><a data-group="design">Design</a></li>
                <li><a data-group="development">Development</a></li>
                <li><a data-group="marketing">Marketing</a></li>
                <li><a data-group="branding">Branding</a></li>
            </ul>
        </div>

        <div class="row">
            <div id="grid">
                <?php foreach($portfolio as $key => $item) { ?>
                <div class="portfolio-item col-xs-12 col-sm-6 col-md-4" data-groups='["all", "marketing", "development", "branding"]'>
                    <div class="single-portfolio">
                        <figure class="css-hover-effect">
                            <?php echo $this->Html->image('../files/projects/'.$item['Project']['image_link'], array('width'=>'360px', 'height'=>'270px'));?>
                            <figcaption class="figure-caption">
                                <div class="figure-link">
                                    <a data-toggle="modal" data-target="#portfolioModal<?php echo $key+1;?>" href="#"><i class="flaticon-slanting1"></i></a>
                                </div>
                                <div class="figure-info">
                                    <span><?php echo $item['Project']['project_type'];?></span>
                                    <h2><a href="#"><?php echo $item['Project']['name'];?></a></h2>
                                </div>
                            </figcaption>
                        </figure>
                    </div><!-- /.single-portfolio -->
                </div>
                <?php } ?>
            </div>
        </div><!-- /.row -->

        <!-- portfolio details in modal, add individual modal for individual item -->
        <!-- Modal -->
        <?php foreach($portfolio as $key => $item) { ?>
        <div class="modal fade portfolioModal" id="portfolioModal<?php echo $key+1;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="flaticon-wrong6"></i></span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <?php echo $this->Html->image('../files/projects/'.$item['Project']['image_link'], array('class'=>'img-responsive'));?>
                            </div>
                            <div class="col-md-6">
                                <div class="portfolio-details">
                                    <h3><?php echo $item['Project']['name'];?></h3>
                                    <p><?php echo $item['Project']['short_desc'];?></p>
                                    <strong>Client: <?php echo $item['Project']['client_name'].", ".$item['Project']['client_organization'];?></strong> <br>
                                    <strong>Platform: <?php echo $item['Project']['project_type'];?></strong> <br>
                                    <strong>Status: <?php echo $item['Project']['status'];?></strong> <br>
                                    <strong>Client Testimonial:</strong> <br>
                                    <blockquote>
                                        <p>
                                        <?php
                                        if(!empty($item['Project']['client_testimonial']) || $item['Project']['client_testimonial']!=" ") {
                                            echo $item['Project']['client_testimonial'];
                                        } else {
                                            echo "Not available yet.";
                                        }
                                        ?>
                                    </blockquote>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.modal-body -->
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.portfolioModal -->
        <?php } ?>


        <div class="more-work-button text-center">
            <a href="#" class="btn btn-primary btn-lg">More Works</a>
        </div>
    </div>
</section>
<!-- work end -->

<!-- team start -->
<section id="team" class="team-section section-padding">
    <div class="container">
        <div class="row text-center">
            <div class="col-xs-12">
                <h2 class="section-title wow zoomIn">Meet The Team</h2>
            </div>
        </div> <!-- /.row -->

        <div class="team-wrapper text-center">
            <div class="row">
                   
                   <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                          <!-- Indicators -->
                          <ol class="carousel-indicators">
                              <?php
                              foreach($team as $key=>$member) {
                                  if($key%3==0) { ?>
                                      <li data-target="#carousel-example-generic"
                                          data-slide-to="<?php echo $key/3;?>"
                                          <?php if($key==0) echo " class=\"active\"";?>  >
                                      </li>
                                  <?php }
                              }
                              ?>
                          </ol>
                          <!-- Wrapper for slides -->
                          <div class="carousel-inner" role="listbox">
                            <?php
                            foreach($team as $key=>$member) {
                                  if($key%3==0) {
                                      if($key==0)
                                          echo "<div class=\"item active\">";
                                      else
                                          echo "<div class=\"item\">";
                                  }
                            ?>
                                <div class="col-sm-4">
                                    <figure class="css-hover-effect">
                                        <?php echo $this->Html->image('../files/team/'.$member['TeamMember']['image_link'], array('height'=>'360px', 'width'=>'360px'));?>
                                        <figcaption class="figure-caption">
                                            <div class="figure-link">
                                                <a class="social-first" href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                                                <a class="social-second" href="#" target="_blank"><i class="fa fa-github-alt"></i></a>
                                            </div>
                                            <div class="figure-info">
                                                <h2><?php echo $member['TeamMember']['display_name']; ?><small><?php echo $member['TeamMember']['designation']; ?></small></h2>
                                            </div>
                                        </figcaption>
                                    </figure>
                                </div><!-- /.col-sm-4 -->
                            <?php
                                  if($key%3==2) {
                                      echo "</div>";
                                  }
                            }
                            ?>
                          </div>

                          <!-- Controls -->
                          <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                          </a>
                          <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                          </a>
                        </div>
            </div><!-- /.row -->
        </div><!-- /.team-wrapper -->
    </div> <!-- /.container -->
</section>
<!-- team end -->

<!-- counter start -->
<section id="countUp" class="counter-section" data-stellar-background-ratio="0.5">
    <div class="css-overlay section-padding">
        <div class="container">
            <div class="row text-center">
                <div class="col-xs-12 col-sm-3">
                    <strong><span class="timer"><?php echo $general_setting['GeneralSetting']['clients'];?></span>+</strong>
                    <span class="count-description">Happy client served</span>
                </div> <!-- /.col-sm-3 -->
                <div class="col-xs-12 col-sm-3">
                    <strong><span class="timer"><?php echo $general_setting['GeneralSetting']['coffee'];?></span>+</strong>
                    <span class="count-description">Cup of coffee</span>
                </div><!-- /.col-sm-3 -->
                <div class="col-xs-12 col-sm-3">
                    <strong><span class="timer"><?php echo $general_setting['GeneralSetting']['projects'];?></span>+</strong>
                    <span class="count-description">Projects Compiled</span>
                </div><!-- /.col-sm-3 -->
                <div class="col-xs-12 col-sm-3">
                    <strong><span class="timer"><?php echo $general_setting['GeneralSetting']['work_hour'];?></span>+</strong>
                    <span class="count-description">Work hour</span>
                </div><!-- /.col-sm-3 -->
            </div>
        </div><!-- /.container -->
    </div><!-- /.css-overlay -->
</section><!-- /.counter-section -->
<!-- counter end -->

<!-- testimonial start -->
<section id="testimonial" class="testimonial-section section-padding">
    <div class="container">
        <div class="row text-center">
            <div class="col-xs-12">
                <h2 class="section-title wow zoomIn">Our Client Say About Us</h2>
            </div>
        </div> <!-- /.row -->

        <div class="row content-row">
            <div class="col-xs-12">
                <div id="testimonial-carousel" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <?php foreach($testimonials as $key => $item) { ?>
                        <li data-target="#testimonial-carousel" data-slide-to="<?php echo $key;?>" <?php if($key==0) echo "class=\"active\"";?>></li>
                        <?php } ?>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <?php foreach($testimonials as $key => $item) { ?>
                        <div class="item <?php if($key==0) echo "active";?>">
                            <?php echo $this->Html->image('../files/client/'.$item['Testimonial']['image_link']);?>
                            <div class="carousel-caption">
                                <p><?php echo $item['Testimonial']['comment']; ?></p>

                                <span class="source-title"><?php echo $item['Testimonial']['name'] .", ". $item['Testimonial']['organization']; ?></span>
                            </div>
                        </div>
                        <?php } ?>
                    </div>

                </div>
            </div>
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</section>
<!-- testimonial end -->

<!-- partner start -->
<section class="partner-section section-padding">
    <div class="container">
        <div class="row text-center">
            <div class="col-xs-12">
                <h2 class="section-title wow zoomIn">We Are Trusted By</h2>
            </div>
        </div> <!-- /.row -->

        <div class="row content-row">
            <?php foreach($partners as $partner) { ?>
            <div class="col-xs-6 col-sm-3 partner wow zoomIn patner">
                <?php
                    echo $this->Html->image('../files/partners/'.$partner['Partner']['logo'], array(
                            "alt" => $partner['Partner']['name'],
                            'url' => $partner['Partner']['link']
                        )
                    );
                ?>
            </div>
            <?php } ?>
            <!--<div class="col-xs-6 col-sm-3 partner wow zoomIn">
                <a href="#"><img src="img/partner/partner-2.png" alt=""></a>
            </div>
            <div class="col-xs-6 col-sm-3 partner wow zoomIn">
                <a href="#"><img src="img/partner/partner-3.png" alt=""></a>
            </div>
            <div class="col-xs-6 col-sm-3 partner wow zoomIn">
                <a href="#"><img src="img/partner/partner-4.png" alt=""></a>
            </div>
            <div class="col-xs-6 col-sm-3 partner wow zoomIn">
                <a href="#"><img src="img/partner/partner-4.png" alt=""></a>
            </div>
            <div class="col-xs-6 col-sm-3 partner wow zoomIn">
                <a href="#"><img src="img/partner/partner-3.png" alt=""></a>
            </div>
            <div class="col-xs-6 col-sm-3 partner wow zoomIn">
                <a href="#"><img src="img/partner/partner-2.png" alt=""></a>
            </div>
            <div class="col-xs-6 col-sm-3 partner wow zoomIn">
                <a href="#"><img src="img/partner/partner-1.png" alt=""></a>
            </div>-->
        </div>
    </div> <!-- /.container -->
</section>
<!-- partner end -->

<!-- Twitter start -->
<!--<section id="twitter" class="twitter-feed-section section-padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2 text-center">
                <div class="twitter owl-carousel"></div>
            </div>
        </div>
    </div>
</section>-->
<!-- Twitter end -->

<!-- footer start -->
<footer id="contact" class="footer-widget-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 col-md-3 col-md-offset-1">
                <div class="footer-widget footer-logo">
                    <a href="#home"><?php echo $this->Html->image('../files/logos/'.$general_setting['GeneralSetting']['logo'], array('height'=>'100px','width'=>'100px'));?></a>
                </div><!-- /.col-md-6 -->
            </div><!-- /.col-md-6 -->
            <div class="col-sm-3 col-md-2 col-lg-2">
                <div class="footer-widget">
                    <h3>Social</h3>
                    <ul>
                        <?php foreach($social_links as $item) { ?>
                        <li><a href="<?php echo $item['SocialLink']['link'];?>"><?php echo $item['SocialLink']['name'];?></a></li>
                        <?php } ?>
                    </ul>
                </div><!-- /.footer-widget -->
            </div><!-- /.col-md-2 -->
            <div class="col-sm-3 col-md-3">
                <div class="footer-widget">
                    <h3>Location</h3>
                    <address>
                        <?php echo $contact_information['ContactInformation']['first_address'].', '.$contact_information['ContactInformation']['second_address'] ?><br>
                        <?php echo $contact_information['ContactInformation']['town'].', '.$contact_information['ContactInformation']['country'] ?><br>
                        <!-- Google Map Modal Trigger -->
                        <a class="modal-map" data-toggle="modal" data-target="#cssMapModal" href="#">View on Google Maps</a>
                    </address>


                    <!-- Modal -->
                    <div class="modal fade" id="cssMapModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Our Location</h4>
                                </div>
                                <div class="modal-body">

                                    <div id="googleMap"></div>

                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- End Modal -->

                </div><!-- /.footer-widget -->
            </div><!-- /.col-md-2 -->
            <div class="col-sm-3 col-md-3">
                <div class="footer-widget">
                    <h3>Get in Touch</h3>
                    <a href="#"><?php echo $contact_information['ContactInformation']['phone_1'];?></a>
                    <a href="mailto:#"><?php echo $contact_information['ContactInformation']['mail_1'];?></a>
                    <a class="feedback-modal" data-toggle="modal" data-target="#feedModal" href="#">Send us your feedback</a>

                    <!-- Modal -->
                    <div class="modal fade" id="feedModal" tabindex="-1" role="dialog" aria-labelledby="feedModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="feedModalLabel">Send us your feedback</h4>
                                </div>
                                <div class="modal-body">
                                    <form id="contactForm" action="http://demo3.themerox.com/html/orchid/sendemail.php" method="POST">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="name">Name</label>
                                                    <input id="name" name="name" type="text" class="form-control"  required="" placeholder="Name">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="email">Email address</label>
                                                    <input id="email" name="email" type="email" class="form-control" required="" placeholder="Email">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="phone">Phone</label>
                                                    <input id="phone" name="phone" type="text" class="form-control" placeholder="Phone">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="subject">Subject</label>
                                                    <input id="subject" name="subject" type="text" class="form-control" required="" placeholder="Subject">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group text-area">
                                            <label for="message">Message</label>
                                            <textarea id="message" name="message" class="form-control" rows="6" required="" placeholder="Message"></textarea>
                                        </div>

                                        <button type="submit" class="btn btn-primary">Send Message</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- /.footer-widget -->
            </div><!-- /.col-md-2 -->
        </div><!-- /.row -->

        <div class="row">
            <div class="col-xs-12 text-center">
                <div class="copyright">
                    <p>&copy; 2016 Xor.  All rights reserved.</p>
                </div>
            </div>
        </div>

    </div><!-- /.container -->
</footer>
<!-- footer end -->

<script>
jQuery(document).ready(function($) {

    "use strict";
    //set your google maps parameters
    var $latitude = "<?php echo $contact_information['ContactInformation']['lat']?>", //If you unable to find latitude and longitude of your address. Please visit http://www.latlong.net/convert-address-to-lat-long.html you can easily generate.
    $longitude = "<?php echo $contact_information['ContactInformation']['lng']?>",
    $map_zoom = 16; /* ZOOM SETTING */

    //google map custom marker icon
    var $marker_url = 'img/map-marker.png';

    //we define here the style of the map
    var style = [{
        "stylers": [
            {"hue": "#6145d6"},
            {"saturation": 100},
            {"gamma": 2.15},
            {"lightness": 12}
        ]
    }];

    //set google map options
    var map_options = {
        center: new google.maps.LatLng($latitude, $longitude),
        zoom: $map_zoom,
        panControl: true,
        zoomControl: true,
        mapTypeControl: false,
        streetViewControl: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false,
        styles: style
    }
    //inizialize the map
    var map = new google.maps.Map(document.getElementById('googleMap'), map_options);
    //add a custom marker to the map
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng($latitude, $longitude),
        map: map,
        visible: true,
        icon: $marker_url
    });


    $('#cssMapModal').on('shown.bs.modal', function(){

    google.maps.event.trigger(map, 'resize');
        map.setCenter(new google.maps.LatLng($latitude, $longitude));
    });


});
</script>