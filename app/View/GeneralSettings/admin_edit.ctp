<?php echo $this->Html->css('custom'); ?>
<?php echo $this->element('menu'); ?>
<div class="generalSettings form">
<?php echo $this->Form->create('GeneralSetting', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Edit General Setting'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('company_name');
        if(!empty($this->Form->data['GeneralSetting']['logo'])){
            echo "<div class=\"thumbnail-item\">";
                echo $this->Html->image('/files/logos/' . $this->request->data['GeneralSetting']['logo'], array('alt'=>'picture', 'width'=> '150px','height' => 'auto'));
            echo "</div>";
        }
		echo $this->Form->input('logo', array('type' => 'file'));
		echo $this->Form->input('slogan');
		echo $this->Form->input('about');
		echo $this->Form->input('why_us');
		echo $this->Form->input('copyright_text');
        echo $this->Form->input('clients');
        echo $this->Form->input('coffee');
        echo $this->Form->input('projects');
        echo $this->Form->input('work_hour');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>

    <script>
        $('input[name="data[GeneralSetting][logo]"]').on('change', function(){
            var new_img =  $('input[name="data[GeneralSetting][logo]"]').val();
            $('.thumbnail-item').find('img').attr('src', new_img);
        });
    </script>
</div>
