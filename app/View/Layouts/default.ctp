<?php
$cakeDescription = __d('cake_dev', 'CakePHP: Company CMS');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
        <?php echo $title_for_layout; ?>
	</title>
    <script type="text/javascript">
        var ROOT = '<?php echo $this->Html->url('/',true); ?>';
    </script>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800' rel='stylesheet' type='text/css'>
    <?php
        echo $this->Html->meta('icon');

        echo $this->Html->css('cake.generic');
        echo $this->Html->css('custom');
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');

        echo $this->Html->script(array('jquery-2.1.1.min','custom'));
    ?>
</head>
<body>
	<div id="container">
		<div id="header">
			<h1><?php echo $cakeDescription ?></h1>
		</div>
        <div id="content">

            <?php echo $this->Session->flash(); ?>

            <?php echo $this->fetch('content'); ?>
        </div>
		<div id="footer">
			<?php echo $this->Html->link(
					$this->Html->image('cake.power.gif', array('alt' => $cakeDescription, 'border' => '0')),
					'http://www.cakephp.org/',
					array('target' => '_blank', 'escape' => false, 'id' => 'cake-powered')
				);
			?>
			<p><?php echo $cakeVersion; ?></p>
		</div>
	</div>
	<?php echo $this->element('sql_dump'); ?>
</body>
</html>
