<div class="customOrders view">
<h2><?php echo __('Custom Order'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($customOrder['CustomOrder']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Id'); ?></dt>
		<dd>
			<?php echo h($customOrder['CustomOrder']['customer_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Project Type'); ?></dt>
		<dd>
			<?php echo h($customOrder['CustomOrder']['project_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Order Date'); ?></dt>
		<dd>
			<?php echo h($customOrder['CustomOrder']['order_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Delivery Date'); ?></dt>
		<dd>
			<?php echo h($customOrder['CustomOrder']['delivery_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Requirements Link'); ?></dt>
		<dd>
			<?php echo h($customOrder['CustomOrder']['requirements_link']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($customOrder['CustomOrder']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($customOrder['CustomOrder']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Custom Order'), array('action' => 'edit', $customOrder['CustomOrder']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Custom Order'), array('action' => 'delete', $customOrder['CustomOrder']['id']), array(), __('Are you sure you want to delete # %s?', $customOrder['CustomOrder']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Custom Orders'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Custom Order'), array('action' => 'add')); ?> </li>
	</ul>
</div>
