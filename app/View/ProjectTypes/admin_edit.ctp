<div class="projectTypes form">
<?php echo $this->Form->create('ProjectType'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Project Type'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ProjectType.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('ProjectType.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Project Types'), array('action' => 'index')); ?></li>
	</ul>
</div>
